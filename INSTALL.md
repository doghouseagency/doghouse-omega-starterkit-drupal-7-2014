# Doghouse Drupal 2014 Starterkit

We've decided to use Omega and Susy for all our Drupal projects moving forward.

## Learning Omega

We recommend watching only the following (approx 1hour).

* [Drupal Tutorials - The Complete Guide To Omega 4 #3 - Creating a Sub Theme](https://www.youtube.com/watch?v=mAbNK4A2fKg&index=4&list=PLLnpHn493BHH5nnK2dKE_42l1oXA6Tq6H)
* [Drupal Tutorials - The Complete Guide To Omega 4 #4 - Omega 4 Settings Overview](https://www.youtube.com/watch?v=npt_ffmtrxk&index=6&list=PLLnpHn493BHH5nnK2dKE_42l1oXA6Tq6H)
* [Drupal Tutorials - The Complete Guide To Omega 4 #5 - Exploring Omega 4's Files](https://www.youtube.com/watch?v=NHkMUoxYJ7Y&index=7&list=PLLnpHn493BHH5nnK2dKE_42l1oXA6Tq6H)
* [Drupal Tutorials - The Complete Guide To Omega 4 #9 - Installing Gems and Compiling Sass](https://www.youtube.com/watch?v=XF_LaSfua8Y&index=11&list=PLLnpHn493BHH5nnK2dKE_42l1oXA6Tq6H)
* [Drupal Tutorials - The Complete Guide To Omega 4 #10 - Sass Structure and Style](https://www.youtube.com/watch?v=Va2q-yBbclM&index=12&list=PLLnpHn493BHH5nnK2dKE_42l1oXA6Tq6H) (beginner)
* [Drupal Tutorials - The Complete Guide To Omega 4 #11 - Sass Structure and Style Part 2](https://www.youtube.com/watch?v=FfvB_--QYOI&index=13&list=PLLnpHn493BHH5nnK2dKE_42l1oXA6Tq6H) (advanced)
* [Drupal Tutorials - The Complete Guide To Omega 4 #12 - Setting Up Your Grid With Singularity](https://www.youtube.com/watch?v=huKh08hxf38&index=14&list=PLLnpHn493BHH5nnK2dKE_42l1oXA6Tq6H) (note will be using susy… see susy videos)
* [Drupal Tutorials - The Complete Guide To Omega 4 #13 - Introduction To Breakpoints](https://www.youtube.com/watch?v=lpmqT3Ng86s&index=15&list=PLLnpHn493BHH5nnK2dKE_42l1oXA6Tq6H)
* [Drupal Tutorials - The Complete Guide To Omega 4 #14 - Using Breakpoints To Build A Responsive Grid](https://www.youtube.com/watch?v=regX3snnAqE&index=16&list=PLLnpHn493BHH5nnK2dKE_42l1oXA6Tq6H)
* [Drupal Tutorials - The Complete Guide To Omega 4 #15 - No Query for Css without Media Queries](https://www.youtube.com/watch?v=af1ZpXAf6DY&index=17&list=PLLnpHn493BHH5nnK2dKE_42l1oXA6Tq6H)

## Learning Susy

We recommend watching the entire playlist (approx 1hour):

https://www.youtube.com/playlist?list=PLLnpHn493BHF-TxB5PqpKfJ_XjTwP5utB

> NOTE: Do not use “sudo” to install gems. The videos are wrong about that.

## Assumptions

* git is installed
* drush is installed
* drush default download path is set to `sites/default/modules/contrib`
* your database is ready and empty

## Folder structure

* sites/default/modules/contrib
* sites/default/modules/custom
* sites/default/modules/devtools
* sites/default/modules/patched
* sites/default/themes/contrib
* sites/default/themes/custom

> NOTE: Patched folder contains patched modules along with a README.txt with
  patch details.

## Install rvm

    $ curl -L get.rvm.io | bash -s stable
    $ source ~/.rvm/scripts/rvm
    
    // Install dependencies & ruby
    rvm autolibs enable
    rvm requirements
    rvm install ruby

    // Update to the latest version of rubygems
    rvm rubygems current

***

## New site install

### Core download

    $ drush dl drupal-7 --destination=.
    $ mv drupal-7.xx myproject
    $ cd myproject
    $ git init
    $ git add .
    $ git commit -m “Drupal Core”
    $ git remote add origin STASH-URL
    $ git push -u origin master

### Environment configuration

Replace Drupal's .gitignore with [Doghouse's .gitignore file template for Drupal 7](http://stash.dhmedia.com.au/projects/DDB/repos/gitignore-drupal7-2015/browse/.gitignore)

    $ rm .gitignore
    $ curl --user {stash-user}:{stash-pass} -G http://stash.dhmedia.com.au/projects/DDB/repos/gitignore-drupal7-2015/browse/.gitignore -d raw > .gitignore
    $ git add .gitignore
    $ git commit -m “Configure gitignore with sensible Doghouse defaults”

Add .fridge file to exclude files from deployment.

    $ curl --user {stash-user}:{stash-pass} -G http://stash.dhmedia.com.au/projects/DDB/repos/fridge-drupal7-2015/browse/.fridge -d raw > .fridge
    $ git add .fridge
    $ git commit -m “Configure .fridge with sensible Doghouse defaults”

    $ git mv .htaccess dev.htaccess
    $ vim dev.htaccess (uncomment “RewriteBase /”)
    $ git add dev.htaccess
    $ git commit -m “Configure .htaccess for Doghouse dev environment”
    $ ln -s dev.htaccess .htaccess

    $ cd sites/default
    $ cp default.settings.php settings.dev.php
    $ ln -s settings.dev.php settings.php
    $ cd ../..

### Site install

    $ drush site-install standard \
      --account-name=superuser \
      --db-url=mysql://root:dhm3d14@127.0.0.1/{project-prefix}_dev
    $ git add .
    $ git commit -m “Add settings file for dev”

> NOTE: Copy the user password given by drush.

### Fix permissions
> Note: For some weird reason, I've had to make the default folder writeable.

    $ chmod +w sites/default

### Download and install dev tools

    $ drush dl browsersync --destination=sites/default/modules/devtools -y
    $ drush en browsersync -y

> IMPORTANT! Go read Browsersync's README file to get it working.

### Theme install

    $ drush dl omega -y --destination=sites/default/themes/contrib
    $ git submodule add \
      ssh://git@stash.dhmedia.com.au:7999/dds/starterkit-drupal7-2014.git \
      sites/default/themes/contrib/omega/omega/starterkits/doghouse
      // Yes, omega is in the path twice, that’s normal!
    $ git add .
    $ git commit -m “Download Omega theme & Doghouse Starterkit”

    // You may need to drush cache-clear drush for the owiz command to be found.
    $ drush cache-clear drush

    // You may also need to enable omega for owiz to work.
    $ drush en omega -y

    $ drush owiz
      > MyTheme
      > mytheme
      > 2: Omega
      > 2: Doghouse
      > sites/default/themes/custom
      > keep readmes: n
      > enable theme: y
      > make default: y
      > dl libs: y
      > new site: y

    // You need to remove the .git folder in your sub-theme folder.
    // Do NOT remove .gitignore.
    $ rm -rf sites/default/themes/custom/{mytheme}/.git
    $ git add .
    $ git commit -m “Generated new custom omega subtheme with drush”

> NOTE: Disable "LiveReload" at {hostname}/admin/appearance/settings/{mytheme}

### Download Doghouse UIKit

    $ mkdir sites/default/themes/custom/{mytheme}/sass/vendor
    $ git submodule add \
      ssh://git@stash.dhmedia.com.au:7999/dds/doghouse-uikit.git \
      sites/default/themes/custom/{mytheme}/sass/vendor/doghouse-uikit
    $ git add .
    $ git commit -m “Download Doghouse UIKit”

### Install ruby

> NOTE: Possibly deprecated.

    $ cd sites/default/themes/custom/{mytheme}
      > ruby-1.9.3-p547 is not installed.
      > To install do: 'rvm install ruby-1.9.3-p547'
    $ rvm install ruby-1.9.3-p547
    $ source ~/.rvm/scripts/rvm

### Configure theme

    $ cd sites/default/themes/custom/{mytheme}
    // only need to cd if you’ve moved

    $ bundle install

### Run it!

    $ drush omega-guard
      > #: MyTheme

So now you can edit your SCSS and enjoy BrowserSync. Yay!

