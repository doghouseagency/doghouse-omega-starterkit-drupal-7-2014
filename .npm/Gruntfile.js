'use strict';

module.exports = function (grunt) {

  var cwd = process.cwd();

  grunt.initConfig({
    watch: {
      options: {
        // Make sure the watch task can find this Gruntfile even if the grunt
        // file base is changed.
        cliArgs: ['--gruntfile', require('path').join(cwd, 'Gruntfile.js')]
      },
      sass: {
        files: ['sass/{,**/}*.{scss,sass}'],
        tasks: ['compass'],
        options: {
        }
      },
      registry: {
        files: ['*.info', '{,**}/*.{php,inc}'],
        tasks: ['shell'],
        options: {
        }
      },
      images: {
        files: ['images/**']
      },
      css: {
        files: ['css/{,**/}*.css']
      },
      js: {
        files: ['js/{,**/}*.js'],
        tasks: ['jshint']
      }
    },

    shell: {
      all: {
        command: 'drush cache-clear theme-registry'
      }
    },

    compass: {
      options: {
        config: 'config.rb',
        bundleExec: true,
        force: true
      },
      dev: {
        options: {
          environment: 'development'
        }
      }
    },

    browserSync: {
      dev: {
        bsFiles: {
          src: 'css/**/*.css'
        },
        options: {
          watchTask: true,
          injectChanges: true
        }
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: ['js/{,**/}*.js']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-shell');

  /*
   * Load & Register BrowserSync
   * @see https://www.npmjs.org/package/grunt-browser-sync
   */
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.registerTask('default', ["browserSync", "watch"]);

  // Change the file base because this Gruntfile is in a sub-folder.
  grunt.file.setBase('../');

};
