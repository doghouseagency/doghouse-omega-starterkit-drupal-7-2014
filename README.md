# Doghouse Omega Theme Starter Kit

This is the default Omega theme Starter Kit with some tweaks to suit how we do things.

## Installation

See the INSTALL.md in the root of this repo.

We also recommend you install [BrowserSync](https://www.drupal.org/project/browsersync):

    drush dl browsersync

## Usage

We're using Grunt under the hood, but you can still run everything using Guard.

### Using Grunt
    cd .npm/
    grunt

### Using Guard
    drush omega-guard
This is the default Omega theme Starter Kit with some tweaks to suit how we do things.

## Changes from default
- Custom GemFile setup with all the correct versions that work together
- Custom Gruntfile.js ready to work with BrowserSync
- Swapped SingularityGS for Susy 2
- Included breakpoint
- Added a few more empty stylesheets with some documentation

## Requires
- Omega 4
- Drupal 7
- Ruby +1.9
- Bundler
- Npm
- Grunt

## Credits (alphabetical order)

* Alex Parker
* Jeremy Graham
* Jonathan Melnick
* Jorge Castro

## Contibuting

Talk to one of the maintainers and/or submit a pull request here:

http://stash.dhmedia.com.au/projects/DDMODULES/repos/starterkit-drupal7-2014/pull-requests
